# Contributing
If you work on the code: comment changes, update the 
changelog, and make sure the code runs before making a 
pull. Bad code will be rejected, no matter what. 